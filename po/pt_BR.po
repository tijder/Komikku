msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-05-23 00:05+0200\n"
"PO-Revision-Date: \n"
"Last-Translator: VaGNaroK <indialuz42@gmail.com>\n"
"Language-Team: \n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.3.1\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../komikku/models/database.py:264
msgid "Complete"
msgstr "Completo"

#: ../komikku/models/database.py:265
msgid "Ongoing"
msgstr "Em andamento"

#: ../komikku/models/database.py:266
msgid "Suspended"
msgstr "Suspenso"

#: ../komikku/models/database.py:267
msgid "Hiatus"
msgstr "Falha"

#: ../komikku/models/database.py:755
msgid "Download pending"
msgstr "Transferência pendente"

#: ../komikku/models/database.py:756
msgid "Downloaded"
msgstr "Baixado"

#: ../komikku/models/database.py:757
msgid "Downloading"
msgstr "Baixando"

#: ../komikku/models/database.py:758
msgid "Download error"
msgstr "Erro ao baixar"

#: ../komikku/utils.py:38
msgid "No Internet connection, timeout or server down"
msgstr ""

#: ../komikku/application.py:46 ../komikku/application.py:50
msgid "Komikku"
msgstr "Komikku"

#: ../komikku/downloader.py:165
msgid "Download completed"
msgstr "Transferência finalizada"

#: ../komikku/downloader.py:166 ../komikku/downloader.py:195
#, python-brace-format
msgid "[{0}] Chapter {1}"
msgstr "[{0}] Capítulo {1}"

#: ../komikku/downloader.py:189
#, python-brace-format
msgid "{0}/{1} pages downloaded"
msgstr "{0}/{1} páginas baixadas"

#: ../komikku/downloader.py:191
msgid "error"
msgstr "erro"

#: ../komikku/updater.py:75
msgid "Library update completed"
msgstr "Atualização da biblioteca concluída"

#: ../komikku/updater.py:77
msgid "Update completed"
msgstr "Atualização completa"

#: ../komikku/updater.py:87
msgid "No new chapter found"
msgstr "Nenhum novo capítulo encontrado"

#: ../komikku/updater.py:113
#, python-brace-format
msgid ""
"{0}\n"
"Oops, update has failed. Please try again."
msgstr ""
"{0}\n"
"Opa, atualização falhou. Por favor, tente novamente."

#: ../komikku/updater.py:121
msgid "Library update started"
msgstr "Atualização da biblioteca iniciada"

#: ../komikku/updater.py:123
msgid "Update started"
msgstr "Atualização iniciada"

#: ../komikku/settings_dialog.py:45
msgid "Settings"
msgstr "Configurações"

#: ../komikku/settings_dialog.py:123
msgid "Right to Left ←"
msgstr "Direita para a esquerda ←"

#: ../komikku/settings_dialog.py:124
msgid "Left to Right →"
msgstr "Esquerda pada direita →"

#: ../komikku/settings_dialog.py:125
msgid "Vertical ↓"
msgstr "Vertical ↓"

#: ../komikku/settings_dialog.py:133
msgid "Adapt to Screen"
msgstr "Adaptar a tela"

#: ../komikku/settings_dialog.py:134
msgid "Adapt to Width"
msgstr "Adaptar à largura"

#: ../komikku/settings_dialog.py:135
msgid "Adapt to Height"
msgstr "Adaptar à altura"

#: ../komikku/settings_dialog.py:136
msgid "Original Size"
msgstr "Tamanho original"

#: ../komikku/settings_dialog.py:144
msgid "White"
msgstr "Branco"

#: ../komikku/settings_dialog.py:145
msgid "Black"
msgstr "Preto"

#: ../komikku/settings_dialog.py:265
msgid "Servers Settings"
msgstr "Configurações de servidores"

#: ../komikku/settings_dialog.py:336
msgid "User Account"
msgstr ""

#: ../komikku/settings_dialog.py:350
msgid "Test"
msgstr ""

#: ../komikku/reader/pager/__init__.py:261
msgid "This chapter is inaccessible."
msgstr "Este capítulo está inacessível."

#: ../komikku/reader/pager/__init__.py:452
msgid "There is no previous chapter."
msgstr "Não há capítulo anterior."

#: ../komikku/reader/pager/__init__.py:454
msgid "It was the last chapter."
msgstr "Esse é o último capítulo."

#: ../komikku/reader/pager/page.py:218
msgid "Failed to load image"
msgstr "Falha ao carregar a imagem"

#: ../komikku/reader/pager/page.py:253
msgid "Retry"
msgstr "Repetir"

#: ../komikku/main_window.py:240
msgid "Contributors: Code, Patches, Debugging:"
msgstr "Contribuintes: Código, Patches, Depuração:"

#: ../komikku/main_window.py:267
msgid "Are you sure you want to quit?"
msgstr "Você tem certeza que quer sair?"

#: ../komikku/main_window.py:270
msgid "Some chapters are currently being downloaded."
msgstr "Alguns capítulos estão sendo baixados no momento."

#: ../komikku/main_window.py:272
msgid "Some mangas are currently being updated."
msgstr "Atualmente, alguns mangás estão sendo atualizados."

#: ../komikku/main_window.py:275
msgid "Quit?"
msgstr "Sair?"

#: ../komikku/card.py:85
#, python-brace-format
msgid ""
"NOTICE\n"
"{0} server is not longer supported.\n"
"Please switch to another server."
msgstr ""
"NOTA\n"
"{0} servidor não é mais suportado.\n"
"Por favor, mude para outro servidor."

#: ../komikku/card.py:136 ../komikku/library.py:192
msgid "Delete?"
msgstr "Apagar?"

#: ../komikku/card.py:137
msgid "Are you sure you want to delete this manga?"
msgstr "Você tem certeza que quer apagar este mangá?"

#: ../komikku/card.py:457
msgid "New"
msgstr "Novo"

#: ../komikku/card.py:473
msgid "%m/%d/%Y"
msgstr "%d/%m/%Y"

#: ../komikku/card.py:561
msgid "Reset"
msgstr "Resetar"

#: ../komikku/card.py:563
msgid "Download"
msgstr "Baixar"

#: ../komikku/card.py:565
msgid "Mark as read"
msgstr "Marcar como lido"

#: ../komikku/card.py:567
msgid "Mark as unread"
msgstr "Marcar como não lido"

#: ../komikku/card.py:729
#, python-brace-format
msgid "Disk space used: {0}"
msgstr "Espaço em disco usado: {0}"

#: ../komikku/library.py:193
msgid "Are you sure you want to delete selected mangas?"
msgstr "Tem certeza que deseja excluir os mangás selecionados?"

#: ../komikku/add_dialog.py:145
#, python-brace-format
msgid "{0} manga added"
msgstr "{0} mangá adicionado"

#: ../komikku/add_dialog.py:248
msgid "MOST POPULARS"
msgstr "MAIS POPULARES"

#: ../komikku/add_dialog.py:281
msgid "Oops, search failed. Please try again."
msgstr "Opa, a busca falhou. Por favor tente novamente."

#: ../komikku/add_dialog.py:283
msgid "No results"
msgstr "Sem resultados"

#: ../komikku/add_dialog.py:365
msgid "Oops, failed to retrieve manga's information."
msgstr "Opa, falha ao recuperar as informações do mangá."

#: ../komikku/add_dialog.py:389
#, python-brace-format
msgid "Search in {0}…"
msgstr "Buscar em {0}…"

#: ../data/ui/shortcuts_overview.ui:14
msgctxt "Shortcut window description"
msgid "Application"
msgstr "Aplicação"

#: ../data/ui/shortcuts_overview.ui:18
msgctxt "Shortcut window description"
msgid "Open preferences"
msgstr "Abrir Preferências"

#: ../data/ui/shortcuts_overview.ui:25
msgctxt "Shortcut window description"
msgid "Toggle fullscreen mode"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:32
msgctxt "Shortcut window description"
msgid "Shortcuts"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:41
msgctxt "Shortcut window description"
msgid "Library"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:45
msgctxt "Shortcut window description"
msgid "Add manga"
msgstr "Adicionar mangá"

#: ../data/ui/shortcuts_overview.ui:52
msgctxt "Shortcut window description"
msgid "Select all"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:59
msgctxt "Shortcut window description"
msgid "Search"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:68
msgctxt "Shortcut window description"
msgid "Manga Card"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:72
msgctxt "Shortcut window description"
msgid "Select all chapters"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:81
msgctxt "Shortcut window description"
msgid "Download Manager"
msgstr ""

#: ../data/ui/shortcuts_overview.ui:85
msgctxt "Shortcut window description"
msgid "Select all downloads"
msgstr ""

#. The subtitle of the umbrella sentence in the first start screen. This is a sentence which gives the user a starting point what he can do if he opens the application for the first time.
#: ../data/ui/main_window.ui:56
msgid "An online/offline manga reader"
msgstr "Um leitor de mangá online/offline"

#: ../data/ui/main_window.ui:174 ../data/ui/add_dialog.ui:267
msgid "Authors"
msgstr "Autor"

#: ../data/ui/main_window.ui:202 ../data/ui/add_dialog.ui:295
msgid "Genres"
msgstr "Gênero"

#: ../data/ui/main_window.ui:230 ../data/ui/add_dialog.ui:323
msgid "Status"
msgstr "Estado"

#: ../data/ui/main_window.ui:258 ../data/ui/add_dialog.ui:351
msgid "Server"
msgstr "Servidor"

#: ../data/ui/main_window.ui:287 ../data/ui/add_dialog.ui:379
msgid "Synopsis"
msgstr "Sinopse"

#: ../data/ui/main_window.ui:317
msgid "Last update"
msgstr "Última atualização"

#: ../data/ui/main_window.ui:372 ../data/ui/add_dialog.ui:410
msgid "Scanlators"
msgstr "Tradutores"

#: ../data/ui/main_window.ui:392
msgid "Info"
msgstr "Informação"

#: ../data/ui/main_window.ui:423
msgid "Chapters"
msgstr "Capítulos"

#: ../data/ui/main_window.ui:572 ../data/ui/settings_dialog.ui:356
msgid "Library"
msgstr "Biblioteca"

#: ../data/ui/menu/main.xml:7
msgid "Update Library"
msgstr "Atualizar Biblioteca"

#: ../data/ui/menu/main.xml:11 ../data/ui/download_manager_dialog.ui:26
msgid "Download Manager"
msgstr "Gerenciador de download"

#: ../data/ui/menu/main.xml:17
msgid "Preferences"
msgstr "Preferências"

#: ../data/ui/menu/main.xml:21
msgid "Keyboard Shortcuts"
msgstr "Atalhos do Teclado"

#: ../data/ui/menu/main.xml:25
msgid "About Komikku"
msgstr "Sobre o Komikku"

#: ../data/ui/menu/library_selection_mode.xml:7 ../data/ui/menu/card.xml:11
msgid "Update"
msgstr "Atualizar"

#: ../data/ui/menu/library_selection_mode.xml:11
#: ../data/ui/menu/download_manager_selection_mode.xml:7
#: ../data/ui/menu/card.xml:7
msgid "Delete"
msgstr "Apagar"

#: ../data/ui/menu/library_selection_mode.xml:18
#: ../data/ui/menu/card_selection_mode.xml:26
msgid "Select All"
msgstr "Selecionar Tudo"

#: ../data/ui/menu/card_selection_mode.xml:15
msgid "Mark as Read"
msgstr "Marcar como lido"

#: ../data/ui/menu/card_selection_mode.xml:19
msgid "Mark as Unread"
msgstr "Marcar como não lido"

#: ../data/ui/menu/download_manager.xml:7
msgid "Delete All"
msgstr "Excluir tudo"

#: ../data/ui/menu/reader.xml:7
msgid "Reading direction"
msgstr "Direção de leitura"

#: ../data/ui/menu/reader.xml:30
msgid "Type of Scaling"
msgstr "Tipo de escala"

#: ../data/ui/menu/reader.xml:58 ../data/ui/settings_dialog.ui:706
msgid "Background Color"
msgstr "Cor do fundo"

#: ../data/ui/menu/reader.xml:76
msgid "Crop borders"
msgstr "Corte das bordas"

#: ../data/ui/menu/card.xml:18
msgid "Order of Chapters"
msgstr "Ordem dos capítulos"

#: ../data/ui/menu/card.xml:23
msgid "By Chapter Number (9-0)"
msgstr "Pelo número do capítulo (9-0)"

#: ../data/ui/menu/card.xml:28
msgid "By Chapter Number (0-9)"
msgstr "Pelo número do capítulo (0-9)"

#: ../data/ui/menu/card.xml:38
msgid "Open in Browser"
msgstr "Abrir no Navegador"

#: ../data/ui/add_dialog.ui:46
msgid "Select a server"
msgstr "Selecione o servidor"

#: ../data/ui/about_dialog.ui.in:11
msgid ""
"An online/offline manga reader.\n"
"\n"
"Never forget, you can support the authors\n"
"by buying the official comics when they are\n"
"available in your region/language."
msgstr ""
"Um leitor de mangá online/offline.\n"
"\n"
"Nunca se esqueça, você pode apoiar os autores\n"
"comprando os quadrinhos oficiais quando estão\n"
"disponível em sua região/idioma."

#: ../data/ui/about_dialog.ui.in:17
msgid "Learn more about Komikku"
msgstr "Saiba mais sobre o Komikku"

#: ../data/ui/settings_dialog.ui:65
msgid "General"
msgstr "Geral"

#: ../data/ui/settings_dialog.ui:116
msgid "Dark Theme"
msgstr "Tema Escuro"

#: ../data/ui/settings_dialog.ui:132
msgid "Use dark GTK theme"
msgstr "Usar tema GTK escuro"

#: ../data/ui/settings_dialog.ui:199
msgid "Night Light"
msgstr "Luz Noturna"

#: ../data/ui/settings_dialog.ui:215
msgid "Automatically enable dark theme at night"
msgstr "Ativar automaticamente tema escuro à noite"

#: ../data/ui/settings_dialog.ui:282
msgid "Desktop Notifications"
msgstr "Notificações da área de trabalho"

#: ../data/ui/settings_dialog.ui:298
msgid "Use desktop notifications for downloads"
msgstr "Usar notificações da área de trabalho para downloads"

#: ../data/ui/settings_dialog.ui:405
msgid "Update at Startup"
msgstr "Atualizar na inicialização"

#: ../data/ui/settings_dialog.ui:421
msgid "Automatically update library at startup"
msgstr "Atualizar automaticamente a biblioteca na inicialização"

#: ../data/ui/settings_dialog.ui:487
msgid "Auto Download of New Chapters"
msgstr "Baixar automaticamente novos capítulos"

#: ../data/ui/settings_dialog.ui:503
msgid "Automatically download new chapters"
msgstr "Baixar automaticamente novos capítulos"

#: ../data/ui/settings_dialog.ui:557
msgid "Enable/disable and configure servers"
msgstr "Ativar/desativar e configurar servidores"

#: ../data/ui/settings_dialog.ui:587
msgid "Long Strip Detection"
msgstr "Detecção de faixa longa"

#: ../data/ui/settings_dialog.ui:603
msgid "Automatically detect long vertical strip when possible"
msgstr "Detectar automaticamente faixa vertical longa quando possível"

#: ../data/ui/settings_dialog.ui:662
msgid "Reader"
msgstr "Leitor"

#: ../data/ui/settings_dialog.ui:690
msgid "Reading Direction"
msgstr "Direção de leitura"

#: ../data/ui/settings_dialog.ui:697
msgid "Type of scaling to adapt image"
msgstr "Tipo de dimensionamento para adaptar a imagem"

#: ../data/ui/settings_dialog.ui:698
msgid "Scaling"
msgstr "Dimensionamento"

#: ../data/ui/settings_dialog.ui:735
msgid "Borders Crop"
msgstr "Cortar Bordas"

#: ../data/ui/settings_dialog.ui:752
msgid "Crop white borders of images"
msgstr "Cortar bordas brancas das imagens"

#: ../data/ui/settings_dialog.ui:818
msgid "Fullscreen"
msgstr "Tela cheia"

#: ../data/ui/settings_dialog.ui:835
msgid "Automatically enter fullscreen mode"
msgstr "Entrar automaticamente no modo de tela cheia"

#: ../data/ui/download_manager_dialog.ui:171
msgid "No downloads"
msgstr "Sem downloads"

#: ../data/info.febvre.Komikku.desktop.in:3
msgid "@prettyname@"
msgstr "@prettyname@"

#: ../data/info.febvre.Komikku.desktop.in:7
msgid "@appid@"
msgstr "@prettyname@"

#: ../data/info.febvre.Komikku.desktop.in:13
msgid "manga;reader;viewer;comic;webtoon;scan;offline;"
msgstr "mangá;leitor;visualizador;comic;webtoon;scan;offline;"

#: ../data/info.febvre.Komikku.appdata.xml.in:9
msgid "An online/offline manga reader for GNOME"
msgstr "Um leitor de mangá online/offline para GNOME"

#: ../data/info.febvre.Komikku.appdata.xml.in:11
msgid ""
"An online/offline manga reader for GNOME developed with the aim of being "
"used with the Librem 5 phone"
msgstr ""
"Um leitor de mangá online/offline para GNOME desenvolvido com o objetivo de "
"ser usado com o telefone Librem 5"

#: ../data/info.febvre.Komikku.appdata.xml.in:35
msgid "Valéry Febvre"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:46
msgid "[Library] Faster chapters list rendering"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:47
msgid ""
"[Library] Faster read/unread state toggling with long chapters selections"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:48
msgid "[Library] Reduce memory used by covers"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:49
msgid "[Servers] MangaSee: Fix manga with chapters grouped by seasons"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:50
msgid "[Servers] Jaimini's Box: Several fixes"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:51
msgid "[Servers] MangaLib: Several fixes"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:58
msgid "[Library] Add search by genre (exact match)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:59
msgid ""
"[Library/Chapters] Add range selection: long press on an item then long "
"press on another to select everything in between"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:60
msgid "[Chapters] Fully cached/read chapters are now marked as \"Downloaded\""
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:61
msgid "[Reader] Improve keyboard navigation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:62
msgid "[Reader] Improve pages transition"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:63
msgid "[Servers] Add Crunchyroll (EN): a Premium account is required"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:64
msgid "[Servers] Scantrad France: Add Most populars"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:65
msgid "Improve manga update"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:66
msgid "New icon"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:73
msgid "[Reader] Add 'Original Size' scaling"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:74
msgid "[Reader] Several fixes in keyboard navigation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:75
msgid "[Settings] Library: Add new option 'Auto Download of New Chapters'"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:76
msgid ""
"[Settings] Library: Add new option 'Long Strip Detection' (MangaDex, "
"MangaNelo, Mangakawaii, JapScan and Union Mangás)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:77
msgid "[Servers] JapScan: Fix search and images loading"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:78
msgid "[Servers] MangaLib: Fix images loading"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:79
msgid "[Servers] Submanga: Update of domain"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:85
msgid "[Reader] Fix crash with corrupt or empty images"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:86
msgid "[Settings] Add servers settings"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:87
msgid "[Servers] Add Union Mangás (PT)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:88
msgid "[Servers] Add MangaLib and HentaiLib (RU)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:89
msgid ""
"[Servers] Add Edelgarde Scans, Hunlight Scans, One Shot Scans, Reaper Scans, "
"The Nonames Scans, Zero Scans (EN)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:90
msgid "[Servers] Add Leviatan Scans (EN, ES)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:91
msgid "[Servers] Hatigarm Scans: new server version"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:92
msgid "[Servers] Scantrad France: Fix"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:93
msgid "[Servers] MangaDex: Added ability to log in"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:94
msgid "[Servers] Read Manga: Fixes"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:95
#: ../data/info.febvre.Komikku.appdata.xml.in:101
msgid "Improved manga update"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:100
msgid "[Servers] Mangakawaii: Fixed manga cover"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:106
msgid "[Manga] Fixed the unread action of chapters"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:107
msgid "[Reader] Cursor hiding during keyboard navigation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:108
msgid "[Reader] Arrow scrolling in every reading directions"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:109
msgid "[Reader] Added white borders crop"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:110
msgid "[Servers] Added Kirei Cake (EN)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:111
msgid "[Servers] Added Read Manga, Mint Manga and Self Manga (RU)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:112
msgid "[Servers] Updated Mangakawaii domain"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:113
msgid "[Servers] Fixed JapScan search (using DuckDuckGo Lite)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:114
msgid "Added back navigation with Escape key"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:115
msgid ""
"Added abilility to add manga by ID using keyword \"id:&lt;id&gt;\" (useful "
"with MangaDex)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:116
msgid "Various bugs fixes"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:121
msgid "Added the Dutch translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:126
msgid "Added Desu server (RU)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:127
msgid "Added the Russian translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:128
msgid "Updated the Brazilian Portuguese translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:129
msgid "Fixed a bug in manga updater"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:134
msgid "Fixe a bug in Download Manager"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:139
msgid "Added a Download Manager"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:140
msgid "[Manga] Improved chapters download"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:141
msgid ""
"[Reader] Vertical reading direction: Added scrolling with UP and DOWN keys"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:142
msgid "[Reader] Added a Retry button when a image load failed"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:143
msgid "[Servers] Removed Manga Rock (server has closed)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:144
msgid "[Servers] Minor fix in xkcd"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:145
msgid "[Servers] MangaDex: Added 15 languages"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:150
msgid "[Library] Add an unread badge"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:151
msgid "[Library/Manga] Added \"Select All\" in selection mode"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:152
msgid ""
"[Library/Manga] Now leaves selection mode when no mangas/chapters are "
"selected"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:153
msgid "[Manga] Improved chapters list rendering"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:154
msgid ""
"[Manga] Improved chapters download: add a progress bar and a \"Stop\" button"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:155
msgid "[Settings] General: New option to disable desktop notifications"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:156
msgid "[Servers] Minor fixes in xkcd"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:157
msgid "[Servers] Add MangaDex (EN, ES, FR)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:162
msgid "Jaimini's Box server: Fixed mangas with an adult alert"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:167
msgid "Added MANGA Plus server (EN, ES)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:168
msgid "Added Jaimini's Box"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:173
msgid "Fixed 'Night Light' preference"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:174
msgid "Fixed JapScan server search"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:179
msgid ""
"Pager focus is now correctly restored when menu is closed (useful for keypad "
"navigation)."
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:180
msgid "Happy new year to everyone."
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:185
msgid "Added 'Vertical' reading direction"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:186
msgid "Added 'Night Light' preference"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:191
msgid "Fixed Manganelo and WEBTOON servers"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:196
msgid "Ninemanga: Fixed missing chapters issue (EN, BR, DE, ES, IT)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:197
msgid "Pepper&amp;Carrot: Fixed chapters update"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:198
msgid "Pepper&amp;Carrot: Added missing Cover and Credits pages"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:203
msgid "New preference: Servers languages"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:204
msgid "Bug fixes in Scantrad France and DB Multiverse"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:209
msgid "A bug fix in change of reading direction"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:210
msgid "A bug fix in Pepper&amp;Carrot"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:215
msgid "Added Pepper&amp;Carrot server"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:216
msgid ""
"A free(libre) and open-source webcomic supported directly by its patrons to "
"change the comic book industry!"
msgstr ""
"Um webcomic gratuito (e livre) de código aberto, suportado diretamente por "
"seus usuários para mudar a indústria de quadrinhos!"

#: ../data/info.febvre.Komikku.appdata.xml.in:221
msgid "New preference: Automatically update library at startup"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:222
msgid "Fix in Mangakawaii server (Cloudflare problem)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:227
msgid "Fixes in Mangakawaii server"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:232
msgid "Added Dragon Ball Multiverse (DBM) server"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:233
msgid "Fixes in Japscan server (search still broken)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:238
msgid "New servers: NineManga Russian, Webtoon Indonesia and Webtoon Thai"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:239
msgid ""
"For all servers that allow it, most popular mangas are now offered as "
"starting search results."
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:244
msgid "Improve speed of dates parsing"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:245
msgid "Fixed order of manga chapters for Scantrad France and Central de Mangás"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:250
msgid "Fix keyboard navigation in reader"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:255
msgid "Add a new server: xkcd (English)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:260
msgid "Add the Portuguese (Brazil) translation"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:261
msgid "Add a new server: Central de Mangas (Portuguese)"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:266
msgid "Bug fix: Change the location of the data storage"
msgstr ""

#: ../data/info.febvre.Komikku.appdata.xml.in:271
msgid "First release"
msgstr ""

#~ msgid "No Internet connection or server down"
#~ msgstr "Sem conexão com à Internet ou servidor inoperante"

#~ msgctxt "Shortcut window description"
#~ msgid "Toggle Fullscreen mode"
#~ msgstr "Trocar para tela cheia"

#~ msgid "Restrict servers to selected languages"
#~ msgstr "Restringir servidores para os idiomas selecionados"

#~ msgid "Servers Languages"
#~ msgstr "Idiomas do Servidor"
